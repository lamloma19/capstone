package com.example.tvseries;

import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import androidx.annotation.Nullable;

public class DatabaseHelper extends SQLiteOpenHelper {
    private Context context;
    private static final String DATABASE_NAME = "TVserieslama.db";
    private static final int DATABASE_VERSION = 1;

    private static final String TABLE_NAME = "my_library";
    private static final String COLOUMN_ID = "_id";
    private static final String COLOUMN_TITLE = "series_title";
    private static final String COLOUMN_GENRE = "series_genre";

    public DatabaseHelper(@Nullable Context context) { // to create the database
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context= context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String query = "CREATE TABLE "+TABLE_NAME+" ("+COLOUMN_ID+" INTEGER PRIMARY KEY AUTOINCREMENT, "+COLOUMN_TITLE+" TEXT, "+COLOUMN_GENRE+" TEXT);";
        db.execSQL(query); // when creating the database we execute this query to create the table


    }



    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME);
        onCreate(db);

    }
    public void addSeries(String name,String genre){
        SQLiteDatabase db = this.getWritableDatabase(); // getting database that will be created
        ContentValues cv = new ContentValues(); // object to hold keys and values for what will fill table
        cv.put(COLOUMN_TITLE,name);
        cv.put(COLOUMN_GENRE,genre);
        long result = db.insert(TABLE_NAME,null,cv); // insert query
        if(result==-1){
            Toast.makeText(context,"Failed",Toast.LENGTH_SHORT).show();
        }
        else{
            Toast.makeText(context,"Added Successfully",Toast.LENGTH_SHORT).show();

        }

    }
    public Cursor gettAllData(){ // to get all data from db and save it in cursor to display it
        String query = "SELECT * FROM "+TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor= null;
        if(db!=null){
            cursor = db.rawQuery(query,null);
        }
        return cursor;

    }


}
