package com.example.tvseries;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.BroadcastReceiver;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.BrokenBarrierException;

public class RecActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rec);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);
        ListView rec_list;
        try {
            JSONObject shows = getHTML("https://www.episodate.com/api/most-popular?page=1");
            Log.d("esht","ESHTA");
            //String s = weather + "";
            JSONArray showsArray = shows.getJSONArray("tv_shows");
            String[] allShows = new String[showsArray.length()];

            for(int i =0;i<showsArray.length();i++){
                allShows[i]=showsArray.getJSONObject(i).get("name")+"";
                Log.d("ARRAYSHOWS",allShows[i]);

            }
            //JSONObject first = showsArray.getJSONObject(0);
           // Log.d("OBJ",first.get("name")+"");

            ArrayAdapter<String> itemsAdapter =
                    new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,allShows);
            rec_list = findViewById(R.id.rec_list);
            rec_list.setAdapter(itemsAdapter);

        }
        catch(Exception e){
            Log.e("ERROR",e.toString());
        }
    }

    public static JSONObject getHTML(String urlToRead) throws Exception {

        StringBuilder result = new StringBuilder();
        URL url = new URL(urlToRead);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line;
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }
        JSONObject myObject = new JSONObject(result.toString());
        rd.close();
        return myObject;

    }

}