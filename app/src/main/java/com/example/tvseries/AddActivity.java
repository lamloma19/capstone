package com.example.tvseries;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class AddActivity extends AppCompatActivity {
    EditText name,genre;
    Button add_series;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
        name = findViewById(R.id.show_name);
        genre = findViewById(R.id.genre);
        add_series= findViewById(R.id.add_show);
        add_series.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) { // getting data from text box and sending it to database helper to insert
               DatabaseHelper db = new DatabaseHelper(AddActivity.this);

               db.addSeries(name.getText().toString().trim(),genre.getText().toString().trim());
            }
        });

    }
}