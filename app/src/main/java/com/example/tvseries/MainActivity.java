package com.example.tvseries;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    FloatingActionButton add_button,rec_button;
    DatabaseHelper db;
    ArrayList name;
    ArrayList genre,id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // REGISTER BROADCAST RECEIVER
        MyReceiever mR = new MyReceiever();
        IntentFilter intent = new IntentFilter();
        intent.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        intent.addAction("android.net.wifi.WIFI_STATE_CHANGED");
        registerReceiver(mR,intent);
        Intent service = new Intent(this,BGMusicService.class);
        startService(service);



        recyclerView = findViewById(R.id.recyclerView);
        add_button= findViewById(R.id.add_button);
        add_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
               Intent intent = new Intent(MainActivity.this,AddActivity.class);
               startActivity(intent); // go to the other screen when we click button through intent
            }
        });

        rec_button= findViewById(R.id.rec_button);
        rec_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,RecActivity.class);
                startActivity(intent); // go to the other screen when we click button through intent
            }
        });
        CustomAdapter CA;

        db = new DatabaseHelper(MainActivity.this);
        name = new ArrayList<>();
        genre = new ArrayList<>();

        insertInArrays();
        CA = new CustomAdapter(MainActivity.this,name,genre);
        recyclerView.setAdapter(CA);
        recyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this));



    }
    public void insertInArrays(){
        Cursor c = db.gettAllData();
        if(c.getCount()==0){
            Toast.makeText(MainActivity.this,"No Data Yet.",Toast.LENGTH_SHORT).show();

        }
        else{
            while (c.moveToNext()){
                Log.d("inLAMA","INN");

                Log.d("inLAMA","INN");

                name.add(c.getString(1));
                genre.add(c.getString(2));

            }
        }
    }
    public void onDestroy(){
        super.onDestroy();
        //stopService(new Intent(this,BGMusicService.class));
    }


}