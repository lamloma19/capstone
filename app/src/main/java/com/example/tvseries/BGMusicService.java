package com.example.tvseries;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.provider.Settings;

import androidx.annotation.Nullable;

public class BGMusicService extends Service {
    private MediaPlayer player;
    @Nullable
    @Override

    public IBinder onBind(Intent intent) {
        return null;
    }
    public int onStartCommand(Intent intent,int flags,int startId){
        if(player==null) {
            player = MediaPlayer.create(this, R.raw.bgmusic);

            //player.setLooping(true);

        }
        player.start();
        return START_STICKY;

    }
    public void onDestroy(){
        super.onDestroy();
        player.stop();
    }
}
