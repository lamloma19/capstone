package com.example.tvseries;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.MyViewHolder> {
   private Context context;
    private ArrayList nameA,genreA;
    public CustomAdapter(Context context,ArrayList nameA,ArrayList genreA){
        Log.d("inCON","INNNN");
        this.context= context;
        this.nameA = nameA;
        this.genreA = genreA;



    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Log.d("inJM","INNNN");
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.my_row,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomAdapter.MyViewHolder holder, int position) {
        Log.d("inYOON","INNNN");
        holder.name.setText(String.valueOf(nameA.get(position)));
        holder.genre.setText(String.valueOf(genreA.get(position)));


    }

    @Override
    public int getItemCount() {
        return nameA.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        TextView name,genre,id;

        public MyViewHolder(@NonNull View itemView) {

            super(itemView);
            Log.d("inCON2","INNNN");
            name = itemView.findViewById(R.id.series_name);
            genre = itemView.findViewById(R.id.series_genre);

        }
    }
}
