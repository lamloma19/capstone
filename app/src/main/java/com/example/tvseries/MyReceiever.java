package com.example.tvseries;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

public class MyReceiever extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) { // will receive the connectivity status from system itself
       // if(intent.getAction().equals())
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if(networkInfo!=null){
            if(networkInfo.getType()==cm.TYPE_MOBILE){
                Toast.makeText(context,"Connected to Data",Toast.LENGTH_SHORT).show();
            }
            if(networkInfo.getType()==cm.TYPE_WIFI){
                Toast.makeText(context,"Connected to WIFI",Toast.LENGTH_SHORT).show();
            }



        }
        else{
            Toast.makeText(context,"Not Connected to a Network",Toast.LENGTH_SHORT).show();

        }


    }
}
