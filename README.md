# capstone

I will develop a tv series application where the user can add dramas they want to watch later along with their genre ( like a bookmark app for tv series) and there will be also a tv series recommendation functionality where i will use a web service for this by fetching top tv series from the internet and displaying them from the user. SQLite will be used to save the tv series that the user enters (content provider) and do some queries on them like delete or update the stored data. There will be 3 activities (3 screens), the main one displays the previously added tv series, an add button to allow user to add a new tv series to the list, a recommendation button to show the tv series recommendation fetched from the web for the user. the second activity will be the add series screen so the user can enter the information needed. the third screen will be the recommendation screen with the fetched data from the url.

The needed requirements:
1) Activity: 3 activities will be developed as mentioned above.
2) Service: i will use background music as a service which is a started service.
3) broadcast receiver: i will use broadcast receivers to send intents and information between activities to add tv series to the list.
4) content provider: SQLite database to save tv series list and do other queries.
5) web service: i will use this url to fetch top rated tv series for recommendations : https://www.episodate.com/api/most-popular?page=1
